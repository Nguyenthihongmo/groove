import { UserProfileModel } from '../account/user-profile/user-profile.model';

export interface UserProfileState {
   userProfile: UserProfileModel;
}
